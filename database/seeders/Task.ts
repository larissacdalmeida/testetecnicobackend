import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Task from 'App/Models/Task'

export default class TaskSeeder extends BaseSeeder {
  public async run () {
    await Task.createMany([
      {
        "title":"Prova de mecânica dos Sólidos",
        "description":"Dia 15 de setembro de 2023",
        "status":false
      },
      {
        "title":"Prova de Eletrônica I",
        "description":"Dia 25 de setembro de 2023",
        "status":false
      },
      {
        "title":"Prova de Modelagem",
        "description":"Dia 25 de setembro de 2023",
        "status":false
      },
      {
        "title":"Trabalho de Desenvolvimento de Jogos",
        "description":"Dia 11 de setembro de 2023",
        "status":true
      },
      {
        "title":"Compressão de imagens",
        "description":"Comprimir imagens pelo node.js express para até 5MB",
        "status":true
      }
    ])
  }
}
