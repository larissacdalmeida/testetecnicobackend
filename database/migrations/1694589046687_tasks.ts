import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Tasks extends BaseSchema {
  protected tableName = 'tasks'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title')
      table.text('description')
      table.boolean('status')
      table.timestamp('created_date')
      table.timestamp('updated_date')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
