import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Task from 'App/Models/Task'
import { schema, rules } from '@ioc:Adonis/Core/Validator'
//import Validator from 'Validator'

export default class TasksController {
  //Listar todas as tarefas
  public async index({}: HttpContextContract) {
    const posts = await Task.all()

    return posts
  }
  //Criar uma nova tarefa
  public async store({request, response}: HttpContextContract) {
    const dataBody = request.body()
    
    const taskSchema = schema.create({
      title: schema.string({trim: true},[
        rules.maxLength(30),
        rules.minLength(3)
      ]),
      description: schema.string.optional({trim: true}),
      status: schema.boolean.optional(),
      created_data: schema.date.optional(),
      update_data: schema.date.optional(),
      id: schema.number.optional([rules.exists({table:'tasks', column: 'id'})])
    })
    await request.validate({schema:taskSchema})

    const newTask = new Task();
    newTask.fill(dataBody)
    await newTask.save()

    return response.json({newTask})
  }
  //Ler uma tarefa específica
  public async show({params}: HttpContextContract) {
    const post = await Task.findOrFail(params.id)

    return post
  }
  //Atualizar uma tarefa existente
  public async update({request, params, response}: HttpContextContract) {
    const post = await Task.findOrFail(params.id)
    const dataBody = request.body()
    
    const taskSchema = schema.create({
      title: schema.string({trim: true},[
        rules.maxLength(30),
        rules.minLength(3)
      ]),
      description: schema.string.optional({trim: true}),
      status: schema.boolean.optional(),
      created_data: schema.date.optional(),
      update_data: schema.date.optional(),
      id: schema.number.optional([rules.exists({table:'tasks', column: 'id'})])
    })
    await request.validate({schema:taskSchema})

    const newTask = new Task();
    newTask.fill(dataBody)
    post.merge(dataBody)

    await post.save()

    return response.json({post})
  }
  //Excluir uma tarefa
  public async destroy({params}: HttpContextContract) {
    const post = await Task.findOrFail(params.id)

    await post.delete()
  }
}
